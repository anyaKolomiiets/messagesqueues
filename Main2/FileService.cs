﻿namespace Main
{
    public class FileService
    {
        private const string FilePath = @"D:\cources\MessageQueues\recieved";
        public void WriteToFile(Message message, int sequenceIndex, byte[] content)
        {
            var outputFile = Path.Combine(FilePath, message.FileName);

            using (var fileStream = new FileStream(outputFile, sequenceIndex == 0 ? FileMode.Create : FileMode.Append))
            {
                fileStream.Write(content, 0, content.Length);
            }
        }
    }
}
