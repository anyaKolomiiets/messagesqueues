﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;

namespace Main
{
    public class RabbitMQConsumer
    {
        private const string ConnectionUri = "amqp://guest:guest@localhost:5672";
        private const string ExchangeName = "test";
        private const string QueueName = "test2";

        private readonly IModel _channel;
        private readonly IConnection _connection;
        private readonly EventingBasicConsumer _consumer;
        private readonly FileService _fileService;
        public RabbitMQConsumer()
        {
            var factory = new ConnectionFactory() { Uri = new Uri(ConnectionUri) };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.QueueDeclare(QueueName, true, false, false);
            _channel.QueueBind(QueueName, ExchangeName, "");

            _consumer = new EventingBasicConsumer(_channel);
            _fileService = new FileService();
        }

        public void RecieveMessages()
        {
            _consumer.Received += (model, eventArgs) =>
            {
                var sequenceId = Encoding.UTF8.GetString((byte[])eventArgs.BasicProperties.Headers["SequenceId"]);
                var sequenceLength = (int)eventArgs.BasicProperties.Headers["SequenceLength"];
                var sequenceIndex = (int)eventArgs.BasicProperties.Headers["SequenceIndex"];

                var messageJson = Encoding.UTF8.GetString(eventArgs.Body.ToArray());
                var message = JsonConvert.DeserializeObject<Message>(messageJson);

                if(message == null)
                {
                    throw new ArgumentNullException(nameof(message));
                }

                var content = message.Content.ToArray();

                _fileService.WriteToFile(message, sequenceIndex, content);

                _channel.BasicAck(eventArgs.DeliveryTag, false);
            };
            var autoAck = false;

            _channel.BasicConsume(QueueName, autoAck, _consumer);
        }
        public void Dispose()
        {
            _connection.Dispose();
            _channel?.Dispose();
        }
    }
}
