﻿using Main;
using Main2;
using Newtonsoft.Json;
using System.Text;

namespace CaptureService
{
    public class FileService : IDisposable
    {
        const string FilePath = @"D:\cources\MessageQueues\filesToSend";
        private const int MaxMessageSize = 10000000;
        private readonly RabbitMQProducer messageProducer;
        public FileService()
        {
            messageProducer = new RabbitMQProducer();
        }

        public void Dispose()
        {
            messageProducer.Dispose();
        }

        public void RecieveAndSendMessage(string file)
        {
            var sequenceId = Guid.NewGuid().ToString();
            var content = File.ReadAllBytes(file);
            var sequenceLength = (int)Math.Ceiling((double)content.Length / MaxMessageSize);

            for (int i = 0; i < sequenceLength; i++)
            {
                var startIndex = i * MaxMessageSize;
                var length = Math.Min(MaxMessageSize, content.Length - startIndex);

                var message = new FileMessage
                {
                    FileName = Path.GetFileName(file),
                    Content = new ArraySegment<byte>(content, startIndex, length)
                };

                var messageJson = JsonConvert.SerializeObject(message);
                var messageBody = Encoding.UTF8.GetBytes(messageJson);

                messageProducer.SendMessage(messageBody, sequenceId,sequenceLength,i);

            }
        }
    }
}
