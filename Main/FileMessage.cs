﻿namespace Main2
{
    public class FileMessage
    {
        public string FileName { get; set; }
        public ArraySegment<byte> Content { get; set; }
    }
}
