﻿// See https://aka.ms/new-console-template for more information

using CaptureService;

namespace Main;
class Program
{

    const string FilePath = @"D:\cources\MessageQueues\filesToSend";

    static FileService fileService = new FileService();
    static void Main(string[] args)
    {
        using var watcher = new FileSystemWatcher(FilePath);

        watcher.NotifyFilter = NotifyFilters.Attributes
                             | NotifyFilters.CreationTime
                             | NotifyFilters.DirectoryName
                             | NotifyFilters.FileName
                             | NotifyFilters.LastAccess
                             | NotifyFilters.LastWrite
                             | NotifyFilters.Security
                             | NotifyFilters.Size;

        watcher.Created += OnCreated;

        watcher.IncludeSubdirectories = true;
        watcher.EnableRaisingEvents = true;

        Console.WriteLine("Press enter to exit.");
        Console.ReadLine();
        fileService.Dispose();
    }

    static void OnCreated(object sender, FileSystemEventArgs e)
    {
        string value = e.FullPath;

        Console.WriteLine($"Processing {value}");

        fileService.RecieveAndSendMessage(value);

        Console.WriteLine($"Sent {value}");
    }
}