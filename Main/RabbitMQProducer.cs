﻿using RabbitMQ.Client;

namespace Main
{
    public class RabbitMQProducer : IDisposable
    {
        private const string ConnectionUri = "amqp://guest:guest@localhost:5672";
        private const string ExchangeName = "test";
        private readonly IModel _channel;
        private readonly IConnection _connection;
        public RabbitMQProducer()
        {
            var factory = new ConnectionFactory() { Uri = new Uri(ConnectionUri) };
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            _channel.ExchangeDeclare(ExchangeName, ExchangeType.Fanout, true);
        }
        public void Dispose()
        {
            _connection.Dispose();
            _channel?.Dispose();
        }

        public void SendMessage(byte[] messageBody, string sequenceId, int sequenceLength, int index)
        {

            var properties = _channel.CreateBasicProperties();
            properties.Persistent = true;
            properties.Headers = new Dictionary<string, object>
            {
                { "SequenceId", sequenceId },
                { "SequenceLength", sequenceLength },
                { "SequenceIndex", index }
            };

            _channel.BasicPublish(ExchangeName, "", properties, messageBody);

        }
    }
}
